package generics.common_g;

import generics.lab1_g.matrix.Matrix;

/**
 * Created by Cristina on 10/16/2016.
 */
public interface IMatrix<T> {
    /***
     * Pre: this.rows = lab1_g.matrix.rows
     *      this.cols = lab1_g.matrix.cols
     * Post: result.rows = this.rows ( = lab1_g.matrix.rows )
     *       result.cols = this.cols ( = lab1_g.matrix.cols )
     * @param matrix - 2d array - type: int
     * @return the sum between lab1_g.matrix and 'this' lab1_g.matrix
     */
    Matrix<T> add(Matrix<T> matrix);

    /***
     * Pre: this.cols = lab1_g.matrix.rows
     * Post: result.rows = this.rows
     *       result.cols = lab1_g.matrix.cols
     * @param matrix - 2d array - type: int
     * @return the product between lab1_g.matrix and 'this' lab1_g.matrix
     */
    Matrix<T> multiply(Matrix<T> matrix);
}

package generics.common_g;

import lab1.matrix.Matrix;

import java.io.*;
import java.util.Random;

public class Utils {

    // generates the same sequence of random numbers
    // the Random() constructor parameter is the seed, a number used to initialize a pseudo number generator
    // mai mult control asupra secventei de nr random
    private static Random r = new Random(0xfeedcafeL);
    private static final int ROWS = 2000, COLS = 2000, MAX_VALUE = 21;

    public static void generateRandomMatrix(String fileName) {
        int[][] resultMatrix = new int[ROWS][COLS];
        int value;
        String resultMatrixString = "";
        for (int i = 0; i < ROWS; i++) {
            for (int j = 0; j < COLS; j++) {
                value = r.nextInt(MAX_VALUE);
                resultMatrix[i][j] = value;
                resultMatrixString += value + " ";
            }
            resultMatrixString += "\n";
        }
        writeMatrixToFile(fileName, resultMatrixString);
    }


    public static Matrix readMatrixFromFile(int rows, int cols, String fileName) {
        int row = 0;
        String currentLine;
        String[] currentLineArray;
        Matrix matrix = new Matrix(rows, cols);
        BufferedReader bufferedReader;
        try {
            bufferedReader = new BufferedReader(new FileReader(fileName));
            while (row < rows) {
                currentLine = bufferedReader.readLine();
                currentLineArray = currentLine.split(" ");
                for (int col = 0; col < cols; col++) {
                    matrix.setElem(row, col, Integer.parseInt(currentLineArray[col]));
                }
                row++;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return matrix;
    }


    private static void writeMatrixToFile(String fileName, String text) {
        BufferedWriter writer;
        try {
            writer = new BufferedWriter(new FileWriter(fileName));
            writer.write(text);
            writer.flush();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public static void printTime(long timeStartClasic, long timeEndClasic, long timeStartThreads, long timeEndThreads) {
//        System.out.println("timeStartClasic = " + timeStartClasic);
//        System.out.println("timeEndClasic = " + timeEndClasic);
        System.out.println("Time Clasic: (timeEndClasic - timeStartClasic) = " + (timeEndClasic - timeStartClasic) + " millisTime");
//        System.out.println("Time Clasic: (timeEndClasic - timeStartClasic) = " + (timeEndClasic - timeStartClasic) + " nanoTime");
//        System.out.println("timeStartThreads = " + timeStartThreads);
//        System.out.println("timeEndThreads = " + timeEndThreads);
        System.out.println("Time using Threads: (timeEndThreads - timeStartThreads) = " + (timeEndThreads - timeStartThreads) + " millisTime");
//        System.out.println("Time using Threads: (timeEndThreads - timeStartThreads) = " + (timeEndThreads - timeStartThreads) + " nanoTime");
    }


}

package generics.lab1_g.matrix;

import generics.common_g.IMatrix;
import lab2.operator.AddOperator;
import lab2.operator.Operator;

import java.lang.reflect.Array;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;


public class Matrix<T> implements IMatrix<T> {
    private T[][] doubleArray;
    private int rows;
    private int cols;
    private Class<T> cls; //tipul de date -> T-ul (Integer, Doble, Complex, etc)

    public Matrix(int rows, int cols, Class<T> cls) {
        this.rows = rows;
        this.cols = cols;
        this.cls = cls;
        doubleArray = (T[][]) Array.newInstance(this.cls, rows, cols);
    }

//    public Matrix(ParallelMatrix paralelMatrixA) {
////        doubleArray = paralelMatrixA.getArray2d();
//        rows = paralelMatrixA.getRows();
//        cols = paralelMatrixA.getCols();
//    }

    @Override
    public Matrix<T> add(Matrix<T> matrix) {
        Operator<T> operator = (Operator<T>) new AddOperator();
        int rows = matrix.getRows(); // == rowsThis = rowsResult
        int cols = matrix.getCols(); // == colsThis = colsResult
        Matrix resultMatrix = new Matrix(rows, cols, cls);

        for (int i = 0; i < rows; i++)
            for (int j = 0; j < cols; j++)
                resultMatrix.setElem(i, j, operator.apply(matrix.getElem(i, j), this.getElem(i, j)));

        return resultMatrix;
    }

    @Override
    public Matrix<T> multiply(Matrix<T> matrixB) {
        Operator<T> addOperator = (Operator<T>) new AddOperator();
        int rowsA = this.getRows();
        int colsA = this.getCols();
        int colsB = matrixB.getCols();
        Matrix<T> matrixC = new Matrix<T>(rowsA, colsB, cls);

        for (int i = 0; i < rowsA; i++) {
            for (int j = 0; j < colsB; j++) {
                T sum = addOperator.initialize();
                T prod = this.getElem(i, j);
                for (int k = 0; k < colsA; k++) {
                    sum = addOperator.apply(prod, addOperator.apply(this.getElem(i, k), matrixB.getElem(k, j)));
                }
                matrixC.setElem(i, j, sum);
            }
        }
        return matrixC;
    }

    @Override
    public String toString() {
        String stringMatrix = "";
        for (int i = 0; i < this.getRows(); i++) {
            for (int j = 0; j < this.getCols(); j++) {
                stringMatrix += this.getElem(i, j) + " ";
            }
            stringMatrix += "\n";
        }
        return stringMatrix;
    }

    public T[][] getDoubleArray() {
        return doubleArray;
    }

    public void setDoubleArray(T[][] doubleArray) {
        this.doubleArray = doubleArray;
    }

    public int getRows() {
        return rows;
    }

    public int getCols() {
        return cols;
    }

    public void setElem(int row, int col, T value) {
        doubleArray[row][col] = value;
    }

    public T getElem(int row, int col) {
        return doubleArray[row][col];
    }

}

package lab1.matrix;

/**
 * Created by Cristina on 10/14/2016.
 */
public class ThreadAddMatrix extends Thread {
    private int startRow;
    private int endRow;
    private Matrix matrixA;
    private Matrix matrixB;
    private Matrix resultMatrix;

    public ThreadAddMatrix(Matrix matrixA, Matrix matrixB, Matrix resultMatrix, int startRow, int endRow) {
        this.matrixA = matrixA;
        this.matrixB = matrixB;
        this.resultMatrix = resultMatrix;
        this.startRow = startRow;
        this.endRow = endRow;
    }

    @Override
    public void run() {
//        System.out.println(this.getClass() + " " + this.getName() + " id: " + this.getId());
        for (int i = startRow; i < endRow; i++) {
            for (int j = 0; j < matrixA.getCols(); j++) {
                resultMatrix.setElem(i, j, matrixA.getElem(i, j) + matrixB.getElem(i, j));
            }
        }
    }
}

package lab1.matrix;

import java.util.ArrayList;

/**
 * Created by Cristina on 10/16/2016.
 */
public class ParallelMatrix extends Matrix {
    private int nrThreads;
    private int totalNrOp;
    private int nrOpTh;
    private int rest;
    private ArrayList<Thread> threads;

    public ParallelMatrix(int rows, int cols, int nrThreads) {
        super(rows, cols);
        this.nrThreads = nrThreads;
    }

    public ParallelMatrix(Matrix matrixA, int nrThreads) {
        super(matrixA.getRows(), matrixA.getCols());
        this.nrThreads = nrThreads;
        this.setArray2d(matrixA.getArray2d());
    }

    @Override
    public Matrix add(Matrix matrix) {
        threads = new ArrayList<>();
        int rows = matrix.getRows();
        int cols = matrix.getCols();
        Matrix resultMatrix = new Matrix(rows, cols);
        setSequenceOfMatrix(rows);
        int startRow, endRow = 0;
        for (int threadIndex = 0; threadIndex < nrThreads; threadIndex++) {
            startRow = endRow;
            endRow = endRow + nrOpTh;
            if (rest > 0) {
                endRow++;
                rest--;
            }
            ThreadAddMatrix threadAddMatrix = new ThreadAddMatrix(this, matrix, resultMatrix, startRow, endRow);
            threads.add(threadAddMatrix);
            threadAddMatrix.start();
        }
        joinAll();
        return resultMatrix;
    }

    @Override
    public Matrix multiply(Matrix matrixB) {
        threads = new ArrayList<>();
        int rows = this.getRows();
        int cols = matrixB.getCols();
        Matrix resultMatrix = new Matrix(rows, cols);
        setSequenceOfMatrix(rows);
        int startRow, endRow = 0;
        for (int threadIndex = 0; threadIndex < nrThreads; threadIndex++) {
            startRow = endRow;
            endRow = endRow + nrOpTh;
            if (rest > 0) {
                endRow++;
                rest--;
            }
            ThreadMultiplyMatrix threadMultiplyMatrix = new ThreadMultiplyMatrix(this, matrixB, resultMatrix, startRow, endRow);
            threads.add(threadMultiplyMatrix);
            threadMultiplyMatrix.start();
        }
        joinAll();
        return resultMatrix;
    }

    private void setSequenceOfMatrix(int rows) {
        if (rows < nrThreads) {
            nrThreads = rows;
        }
        totalNrOp = rows;
        nrOpTh = totalNrOp / nrThreads;
        rest = totalNrOp % nrThreads;
    }

    private void joinAll() {
        for (Thread t : threads) {
            try {
                t.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

}

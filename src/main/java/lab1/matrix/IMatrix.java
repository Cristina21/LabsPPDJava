package lab1.matrix;

import lab1.matrix.Matrix;

/**
 * Created by Cristina on 10/16/2016.
 */
public interface IMatrix {
    /***
     * Pre: this.rows = lab1_g.matrix.rows
     *      this.cols = lab1_g.matrix.cols
     * Post: result.rows = this.rows ( = lab1_g.matrix.rows )
     *       result.cols = this.cols ( = lab1_g.matrix.cols )
     * @param matrix - 2d array - type: int
     * @return the sum between lab1_g.matrix and 'this' lab1_g.matrix
     */
    Matrix add(Matrix matrix);

    /***
     * Pre: this.cols = matrix.rows (colsA == rowsB)
     * Post: result.rows = this.rows (resultMatrix => rowsA x colsB)
     *       result.cols = lab1_g.matrix.cols
     * ia cate o secventa randuri din prima matrice, pe un thread, si le multiplica cu toata matricea B
     * this - prima matrice, de tip int
     * @param matrix - 2d array - type: int
     * @return this * matrixB - the product between matrix and 'this'matrix (produsul matricilor)
     */
    Matrix multiply(Matrix matrix);
}

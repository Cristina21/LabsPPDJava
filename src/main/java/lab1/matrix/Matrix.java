package lab1.matrix;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class Matrix implements IMatrix {
    public static final String EMPTY_STRING = "";
    public static final String WHITE_SPACE = " ";
    public static final String NEW_LINE = "\n";
    private int[][] array2d;
    private int rows;
    private int cols;

    public Matrix(int rows, int cols) {
        this.rows = rows;
        this.cols = cols;
        array2d = new int[rows][cols];
    }

    public Matrix(ParallelMatrix paralelMatrixA) {
        array2d = paralelMatrixA.getArray2d();
        rows = paralelMatrixA.getRows();
        cols = paralelMatrixA.getCols();
    }

    @Override
    public Matrix add(Matrix matrix) {
        int rows = matrix.getRows(); // == rowsThis = rowsResult
        int cols = matrix.getCols(); // == colsThis = colsResult
        Matrix resultMatrix = new Matrix(rows, cols);
        for (int i = 0; i < rows; i++)
            for (int j = 0; j < cols; j++)
                resultMatrix.setElem(i, j, (matrix.getElem(i, j) + this.getElem(i, j)));
        return resultMatrix;
    }

    @Override
    public Matrix multiply(Matrix matrixB) {
        assertThat(this.getCols(), equalTo(matrixB.getRows()));
        int rowsA = this.getRows();
        int colsA = this.getCols();
        int colsB = matrixB.getCols();
        Matrix matrixC = new Matrix(rowsA, colsB);
        for (int i = 0; i < rowsA; i++) {
            for (int j = 0; j < colsB; j++) {
                int sum = 0;
                for (int k = 0; k < colsA; k++) {
                    sum += this.getElem(i, k) * matrixB.getElem(k, j);
                }
                matrixC.setElem(i, j, sum);
            }
        }
        return matrixC;
    }

    @Override
    public String toString() {
        StringBuilder stringMatrix = new StringBuilder();
        for (int i = 0; i < this.getRows(); i++) {
            for (int j = 0; j < this.getCols(); j++) {
                stringMatrix.append(this.getElem(i, j) + WHITE_SPACE);
            }
            stringMatrix.append(NEW_LINE);
        }
        return stringMatrix.toString();
    }


    /***
     * TODO: TO TEST -> Benchmark -> To compare with toString() - that uses StringBuilder
     * @return
     */
    public String toString_NoStringBuilder() {
        String stringMatrix = EMPTY_STRING;
        for (int i = 0; i < this.getRows(); i++) {
            for (int j = 0; j < this.getCols(); j++) {
                stringMatrix += this.getElem(i, j) + WHITE_SPACE;
            }
            stringMatrix += NEW_LINE;
        }
        return stringMatrix;
    }

    public int[][] getArray2d() {
        return array2d;
    }

    public void setArray2d(int[][] array2d) {
        this.array2d = array2d;
    }

    public int getRows() {
        return rows;
    }

    public int getCols() {
        return cols;
    }

    public void setElem(int row, int col, int value) {
        array2d[row][col] = value;
    }

    public int getElem(int row, int col) {
        return array2d[row][col];
    }

}

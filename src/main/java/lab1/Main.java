package lab1;

import common.Utils;
import lab1.matrix.ParallelMatrix;

import java.util.Scanner;

/**
 * Created by Cristina on 10/17/2016.
 */
public class Main {
    public static void main(String[] args) {
        try {
            Scanner scanner = new Scanner(System.in);
            while (true) {
                System.out.println("Number of threads: ");
                String nrOfThreadsString = scanner.next();
                int nrOfThreads = Integer.parseInt(nrOfThreadsString);

                //matixA
                System.out.println("Number of rowsA: ");
                String nrOfRowsStringA = scanner.next();
                int rowsA = Integer.parseInt(nrOfRowsStringA);

                System.out.println("Number of colsA: ");
                String nrOfColsStringA = scanner.next();
                int colsA = Integer.parseInt(nrOfColsStringA);

                //matrixB
                System.out.println("Number of rowsB: ");
                String nrOfRowsStringB = scanner.next();
                int rowsB = Integer.parseInt(nrOfRowsStringB);

                System.out.println("Number of colsB: ");
                String nrOfColsStringB = scanner.next();
                int colsB = Integer.parseInt(nrOfColsStringB);

                System.out.println("Menu: \n 1. Add \n 2. Multiply");
                String cmd = scanner.next();
                if (cmd.equals("1")) {
                    ParallelMatrix parallelMatrixA = new ParallelMatrix(Utils.readMatrixFromFile(rowsA, colsA, "matrixA.txt"), nrOfThreads);
                    ParallelMatrix parallelMatrixB = new ParallelMatrix(Utils.readMatrixFromFile(rowsB, colsB, "matrixB.txt"), nrOfThreads);

                    //Timing
                    long timeStart = System.currentTimeMillis();
                    System.out.println(parallelMatrixA.add(parallelMatrixB));
                    long timeEnd = System.currentTimeMillis();
                    System.out.println("Time: " + (timeEnd - timeStart) + " millis");
                } else if (cmd.equals("2")) {
                    ParallelMatrix parallelMatrixA = new ParallelMatrix(Utils.readMatrixFromFile(rowsA, colsA, "matrixA.txt"), nrOfThreads);
                    ParallelMatrix parallelMatrixB = new ParallelMatrix(Utils.readMatrixFromFile(rowsB, colsB, "matrixB.txt"), nrOfThreads);

                    //Timing
                    long timeStart = System.currentTimeMillis();
                    System.out.println(parallelMatrixA.multiply(parallelMatrixB));
                    long timeEnd = System.currentTimeMillis();
                    System.out.println("Time: " + (timeEnd - timeStart) + " millis");
                } else {
                    System.out.println("Wrong command!");
                }
            }
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
    }
}

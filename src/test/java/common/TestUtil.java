package common;

import org.junit.Test;

import java.util.Random;

public class TestUtil {

    // generates the same sequence of random numbers
    // the Random() constructor parameter is the seed, a number used to initialize a pseudo number generator
    // mai mult control asupra secventei de nr random

    @Test
    public void generateRandomMatixInFile() {
        Utils.generateRandomMatrix("matrixA.txt");
        Utils.generateRandomMatrix("matrixB.txt");
    }


}

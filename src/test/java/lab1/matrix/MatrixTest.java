package lab1.matrix;

import org.junit.Assert;
import org.junit.Test;

import static org.hamcrest.core.IsEqual.equalTo;

/**
 * Created by Cristina on 10/16/2016.
 */
public class MatrixTest {
    @Test
    public void add() {
        Matrix matrixA = new Matrix(2, 3);
        Matrix matrixB = new Matrix(2, 3);
        int[][] boardA = {{1, 2, 3}, {1, 2, 3}};
        int[][] boardB = {{2, 2, 2}, {4, 4, 4}};
        int[][] resultBoard = {{3, 4, 5}, {5, 6, 7}};
        matrixA.setArray2d(boardA);
        matrixB.setArray2d(boardB);

        Matrix resultMatrix = matrixA.add(matrixB);

        Assert.assertThat(resultBoard, equalTo(resultMatrix.getArray2d()));
    }

    @Test
    public void multiply() {
        //colsA == rowsB
        Matrix matrixA = new Matrix(2, 3);
        Matrix matrixB = new Matrix(3, 3);
        int[][] boardA = {{1, 2, 3}, {1, 2, 3}};
        int[][] boardB = {{1, 1, 2}, {1, 1, 3}, {1, 1, 4}};
        int[][] resultBoardExpected = {{6, 6, 20}, {6, 6, 20}};
        matrixA.setArray2d(boardA);
        matrixB.setArray2d(boardB);

        Matrix resultMatrixActual = matrixA.multiply(matrixB);

        Assert.assertThat(resultMatrixActual.getArray2d(), equalTo(resultBoardExpected));
    }

}
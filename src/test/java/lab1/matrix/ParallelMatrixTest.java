package lab1.matrix;

import common.Utils;
import org.junit.Assert;
import org.junit.Test;

import static common.Utils.printTime;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;

public class ParallelMatrixTest {
    @Test
    public void addTest_2x3_correctness() {
        ParallelMatrix paralelMatrixA = new ParallelMatrix(2, 3, 2);
        Matrix paralelMatrixB = new Matrix(2, 3);
        int[][] boardA = {{1, 2, 3}, {1, 2, 3}};
        int[][] boardB = {{2, 2, 2}, {4, 4, 4}};
        int[][] resultBoardExpected = {{3, 4, 5}, {5, 6, 7}};
        paralelMatrixA.setArray2d(boardA);
        paralelMatrixB.setArray2d(boardB);

        Matrix resultMatrixActual = paralelMatrixA.add(paralelMatrixB);

        Assert.assertThat(resultMatrixActual.getArray2d(), equalTo(resultBoardExpected));
    }

    @Test
    public void add_2x4_4_correctness() {
        ParallelMatrix paralelMatrixA = new ParallelMatrix(2, 4, 4);
        Matrix matrixB = new Matrix(2, 4);
        int[][] boardA = {{1, 2, 3, 4}, {1, 2, 3, 4}};
        int[][] boardB = {{20, 20, 20, 20}, {40, 40, 40, 40}};
        int[][] resultBoardExpected = {{21, 22, 23, 24}, {41, 42, 43, 44}};
        paralelMatrixA.setArray2d(boardA);
        matrixB.setArray2d(boardB);
        Matrix resultMatrixActual;

        /***
         * Timing
         */
        long timeStartClasic = System.currentTimeMillis();
        matrixB.add(paralelMatrixA);
        long timeEndClasic = System.currentTimeMillis();

        long timeStartThreads = System.currentTimeMillis();
        resultMatrixActual = paralelMatrixA.add(matrixB);
        long timeEndThreads = System.currentTimeMillis();

        printTime(timeStartClasic, timeEndClasic, timeStartThreads, timeEndThreads);
        Assert.assertThat(resultMatrixActual.getArray2d(), equalTo(resultBoardExpected));
    }

    @Test
    public void addTest_10x15_4() {
        int rowsA = 10, colsA = 15, rowsB = 10, colsB = 15; // colsA == rowsB -> mandatory condition
        Matrix matrixA = Utils.readMatrixFromFile(rowsA, colsA, "matrixA.txt");
        Matrix matrixB = Utils.readMatrixFromFile(rowsB, colsB, "matrixB.txt");
        ParallelMatrix parallelMatrixA = new ParallelMatrix(matrixA, 4);
        Matrix resultMatrixExpected, resultMatrixActual;

        /***
         * Timing
         */
        long timeStartClasic = System.currentTimeMillis();
        resultMatrixExpected = matrixA.add(matrixB);
        long timeEndClasic = System.currentTimeMillis();

        long timeStartThreads = System.currentTimeMillis();
        resultMatrixActual = parallelMatrixA.add(matrixB);
        long timeEndThreads = System.currentTimeMillis();

        printTime(timeStartClasic, timeEndClasic, timeStartThreads, timeEndThreads);
        assertThat(resultMatrixActual.getArray2d(), equalTo(resultMatrixExpected.getArray2d()));
    }

    @Test
    public void addTest_1500x1500_5() {
        int rowsA = 1500, colsA = 1500, rowsB = 1500, colsB = 1500; // colsA == rowsB -> mandatory condition
        Matrix matrixA = Utils.readMatrixFromFile(rowsA, colsA, "matrixA.txt");
        Matrix matrixB = Utils.readMatrixFromFile(rowsB, colsB, "matrixB.txt");
        ParallelMatrix parallelMatrixA = new ParallelMatrix(matrixA, 5);
        Matrix resultMatrixExpected, resultMatrixActual;

        /***
         * Timing
         */
        long timeStartClasic = System.currentTimeMillis();
        resultMatrixExpected = matrixA.add(matrixB);
        long timeEndClasic = System.currentTimeMillis();

        long timeStartThreads = System.currentTimeMillis();
        resultMatrixActual = parallelMatrixA.add(matrixB);
        long timeEndThreads = System.currentTimeMillis();

        printTime(timeStartClasic, timeEndClasic, timeStartThreads, timeEndThreads);
        assertThat(resultMatrixActual.getArray2d(), equalTo(resultMatrixExpected.getArray2d()));
    }

    @Test
    public void addTest_2000x2000_8() {
        int rowsA = 2000, colsA = 2000, rowsB = 2000, colsB = 2000; // colsA == rowsB -> mandatory condition
        Matrix matrixA = Utils.readMatrixFromFile(rowsA, colsA, "matrixA.txt");
        Matrix matrixB = Utils.readMatrixFromFile(rowsB, colsB, "matrixB.txt");
        ParallelMatrix parallelMatrixA = new ParallelMatrix(matrixA, 8);
        Matrix resultMatrixExpected, resultMatrixActual;

        /***
         * Timing
         */
        long timeStartClasic = System.currentTimeMillis();
        resultMatrixExpected = matrixA.add(matrixB);
        long timeEndClasic = System.currentTimeMillis();

        long timeStartThreads = System.currentTimeMillis();
        resultMatrixActual = parallelMatrixA.add(matrixB);
        long timeEndThreads = System.currentTimeMillis();

        printTime(timeStartClasic, timeEndClasic, timeStartThreads, timeEndThreads);
        assertThat(resultMatrixActual.getArray2d(), equalTo(resultMatrixExpected.getArray2d()));
    }

    @Test
    public void addTest_2000x2000_15() {
        int rowsA = 2000, colsA = 2000, rowsB = 2000, colsB = 2000; // colsA == rowsB -> mandatory condition
        Matrix matrixA = Utils.readMatrixFromFile(rowsA, colsA, "matrixA.txt");
        Matrix matrixB = Utils.readMatrixFromFile(rowsB, colsB, "matrixB.txt");
        ParallelMatrix parallelMatrixA = new ParallelMatrix(matrixA, 15);
        Matrix resultMatrixExpected, resultMatrixActual;

        /***
         * Timing
         */
        long timeStartClasic = System.currentTimeMillis();
        resultMatrixExpected = matrixA.add(matrixB);
        long timeEndClasic = System.currentTimeMillis();

        long timeStartThreads = System.currentTimeMillis();
        resultMatrixActual = parallelMatrixA.add(matrixB);
        long timeEndThreads = System.currentTimeMillis();

        printTime(timeStartClasic, timeEndClasic, timeStartThreads, timeEndThreads);
        assertThat(resultMatrixActual.getArray2d(), equalTo(resultMatrixExpected.getArray2d()));
    }

    @Test
    public void multiplyTest_2x3_2_correctness() {
        //colsA == rowsB
        ParallelMatrix paralelMatrixA = new ParallelMatrix(2, 3, 2);
        ParallelMatrix paralelMatrixB = new ParallelMatrix(3, 3, 2);
        int[][] boardA = {{1, 2, 3}, {1, 2, 3}};
        int[][] boardB = {{1, 1, 2}, {1, 1, 3}, {1, 1, 4}};
        int[][] resultBoardExpected = {{6, 6, 20}, {6, 6, 20}};
        paralelMatrixA.setArray2d(boardA);
        paralelMatrixB.setArray2d(boardB);
        Matrix resultMatrixActual;

        /***
         * Timing
         */
        Matrix matrixA = new Matrix(paralelMatrixA);
        long timeStartClasic = System.currentTimeMillis();
        Matrix resultMatrixExpected = matrixA.multiply(paralelMatrixB);
        long timeEndClasic = System.currentTimeMillis();

        long timeStartThreads = System.currentTimeMillis();
        resultMatrixActual = paralelMatrixA.multiply(paralelMatrixB);
        long timeEndThreads = System.currentTimeMillis();

        printTime(timeStartClasic, timeEndClasic, timeStartThreads, timeEndThreads);
        assertThat(resultMatrixActual.getArray2d(), equalTo(resultMatrixExpected.getArray2d()));
        Assert.assertThat(resultMatrixActual.getArray2d(), equalTo(resultBoardExpected));
    }

    @Test
    public void multiplyTest_10x15_4() {
        int rowsA = 10, colsA = 15, rowsB = 15, colsB = 12; // colsA == rowsB -> mandatory condition
        Matrix matrixA = Utils.readMatrixFromFile(rowsA, colsA, "matrixA.txt");
        Matrix matrixB = Utils.readMatrixFromFile(rowsB, colsB, "matrixB.txt");
        ParallelMatrix parallelMatrixA = new ParallelMatrix(matrixA, 4);
        Matrix resultMatrixExpected, resultMatrixActual;

        /***
         * Timing
         */
        long timeStartClasic = System.currentTimeMillis();
        resultMatrixExpected = matrixA.multiply(matrixB);
        long timeEndClasic = System.currentTimeMillis();

        long timeStartThreads = System.currentTimeMillis();
        resultMatrixActual = parallelMatrixA.multiply(matrixB);
        long timeEndThreads = System.currentTimeMillis();

        printTime(timeStartClasic, timeEndClasic, timeStartThreads, timeEndThreads);
        assertThat(resultMatrixActual.getArray2d(), equalTo(resultMatrixExpected.getArray2d()));
    }

    @Test
    public void multiplyTest_1000x1500_8() {
        int rowsA = 1000, colsA = 1500, rowsB = 1500, colsB = 1200; // colsA == rowsB -> mandatory condition
        Matrix matrixA = Utils.readMatrixFromFile(rowsA, colsA, "matrixA.txt");
        Matrix matrixB = Utils.readMatrixFromFile(rowsB, colsB, "matrixB.txt");
        ParallelMatrix parallelMatrixA = new ParallelMatrix(matrixA, 8);
        Matrix resultMatrixExpected, resultMatrixActual;

        /***
         * Timing
         */
        long timeStartClasic = System.currentTimeMillis();
        resultMatrixExpected = matrixA.multiply(matrixB);
        long timeEndClasic = System.currentTimeMillis();

        long timeStartThreads = System.currentTimeMillis();
        resultMatrixActual = parallelMatrixA.multiply(matrixB);
        long timeEndThreads = System.currentTimeMillis();

        printTime(timeStartClasic, timeEndClasic, timeStartThreads, timeEndThreads);
        assertThat(resultMatrixActual.getArray2d(), equalTo(resultMatrixExpected.getArray2d()));
    }

    @Test
    public void multiplyTest_2000x2000_15() {
        int rowsA = 2000, colsA = 2000, rowsB = 2000, colsB = 2000; // colsA == rowsB -> mandatory condition
        Matrix matrixA = Utils.readMatrixFromFile(rowsA, colsA, "matrixA.txt");
        Matrix matrixB = Utils.readMatrixFromFile(rowsB, colsB, "matrixB.txt");
        ParallelMatrix parallelMatrixA = new ParallelMatrix(matrixA, 15);
        Matrix resultMatrixExpected, resultMatrixActual;

        /***
         * Timing
         */
        long timeStartClasic = System.currentTimeMillis();
        resultMatrixExpected = matrixA.multiply(matrixB);
        long timeEndClasic = System.currentTimeMillis();

        long timeStartThreads = System.currentTimeMillis();
        resultMatrixActual = parallelMatrixA.multiply(matrixB);
        long timeEndThreads = System.currentTimeMillis();

        printTime(timeStartClasic, timeEndClasic, timeStartThreads, timeEndThreads);
        assertThat(resultMatrixActual.getArray2d(), equalTo(resultMatrixExpected.getArray2d()));
    }

    @Test
    public void multiplyTest_2000x2000_8() {
        int rowsA = 2000, colsA = 2000, rowsB = 2000, colsB = 2000; // colsA == rowsB -> mandatory condition
        Matrix matrixA = Utils.readMatrixFromFile(rowsA, colsA, "matrixA.txt");
        Matrix matrixB = Utils.readMatrixFromFile(rowsB, colsB, "matrixB.txt");
        ParallelMatrix parallelMatrixA = new ParallelMatrix(matrixA, 8);
        Matrix resultMatrixExpected, resultMatrixActual;

        /***
         * Timing
         */
        long timeStartClasic = System.currentTimeMillis();
        resultMatrixExpected = matrixA.multiply(matrixB);
        long timeEndClasic = System.currentTimeMillis();

        long timeStartThreads = System.currentTimeMillis();
        resultMatrixActual = parallelMatrixA.multiply(matrixB);
        long timeEndThreads = System.currentTimeMillis();

        printTime(timeStartClasic, timeEndClasic, timeStartThreads, timeEndThreads);
        assertThat(resultMatrixActual.getArray2d(), equalTo(resultMatrixExpected.getArray2d()));
    }
}
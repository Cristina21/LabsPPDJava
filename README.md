Laborator 1 (PPD) - (Deathline: saptamana 3)
Implementati in Java
A) adunarea a doua matrice de dimensiune (nxm)
B) inmultirea a doua matrice de dimensiune (nxk), respectiv (k,m)
folosind multithreading.
-Numarul de threduri p trebuie sa fie un parametru care poate fi citit (modificat) inainte de
inceperea executiei.
Datele de intrare corespunzatoare elementelor matricilor se vor citi din fisiere (care au fost
anterior create folosind generare aleatoare de numere)!
Programul va afisa la sfarsit timpul global/total T de executie corespunzator operatiei de
adunare(respective operatiei de inmultire) de matrice.
(T = timp_evaluat_ dupa_terminarea_executiei_fiecarui_thread – timp_evaluat_inainte_de pornirea_threadurilor)
Important: folositi o incarcare echilibrata de calcul pe fiecare thread.
Balanced Distribution (dimensiunea datelor pe care lucreaza fiecare thread este aproximativ
egala).

Laborator 2 (PPD) - Deadline: Saptamana 3
Generalizati programul de la laboratorul 1 (pct. A) prin inlocuirea operatiei de
adunare cu un operator asociativ general.
Recomandare:
O posibilitate de definire a operatorului pentru oeranzi de tip ‘double’:
abstract class Operator {
    public abstract double apply(double x, double y);
}
class AddOperator extends Operator{
    public double apply(double x, double y){
        return x+y;
    }
}
class MultiplyOperator extends Operator{
    public double apply(double x, double y){
        return x*y;
    }
}

Cum s-ar putea generaliza si tipul operanzilor?
Implementati un program pentru adunare matrice de numere complexe.
Testati programul corespunzator adunarii matricilor pentru diferiti operatori si
analizati timpul de executie.
